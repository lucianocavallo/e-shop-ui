const productValidator = (
  product: ProductValidation,
  setError: React.Dispatch<React.SetStateAction<string>>
) => {
  if (product.name === '') setError('Name is required');
  if (product.price === 0) setError('Price is required');
  if (product.description === '') setError('Description is required');
  if (product.image_url === '') setError('Image URL is required');
  if (product.brandId === 0) setError('Brand is required');
  if (
    product.name.length > 0 &&
    product.image_url.length > 0 &&
    product.description.length > 0 &&
    product.price > 0 &&
    product.brandId > 0
  )
    return true;
  return false;
};

export { productValidator };
