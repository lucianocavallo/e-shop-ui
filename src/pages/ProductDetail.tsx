import axios from 'axios';
import { useEffect, useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { HiArrowSmLeft } from 'react-icons/hi';

const url = process.env.REACT_APP_API_URL;

const ProductDetail: React.FC = () => {
  const navigate = useNavigate();
  const { id } = useParams();
  const [product, setProduct] = useState<Product | null>(null);

  const handleGoBack = () => {
    navigate(-1);
  };

  useEffect(() => {
    (async () => {
      const { data } = await axios.get(`${url}products/${id}`);
      setProduct(data);
    })();
  }, [id]);

  return (
    <section className="flex flex-col mx-auto px-4 mt-20 sm:px-10 lg:flex-row lg:w-[800px] lg:mx-auto lg:p-0 relative">
      <button onClick={handleGoBack} className="absolute -top-10">
        <HiArrowSmLeft size={40} />
      </button>
      <figure className="lg:w-1/2">
        <img
          src={product?.image_url}
          alt=""
          className="w-full h-full object-cover"
        />
      </figure>
      <div className="lg:w-1/2 mt-4 lg:mt-0 lg:ml-4">
        <h3>
          Product: <span className="font-bold">{product?.name}</span>
        </h3>
        <p>
          Price: <span className="font-bold">${product?.price}</span>
        </p>
        <p>
          Description: <span className="font-bold">{product?.description}</span>
        </p>
        <div className="flex items-center justify-center">
          <span className="capitalize">Product Brand: </span>
          <figure className="w-36 h-36">
            <img
              src={product?.brand.image_url}
              alt=""
              className="w-full h-full object-contain"
            />
          </figure>
        </div>
      </div>
    </section>
  );
};

export default ProductDetail;
