import React, {
  FormEventHandler,
  useRef,
  MutableRefObject,
  useEffect,
} from 'react';
import axios from 'axios';
import { useAppDispatch } from '../redux/hooks';
import { addUser } from '../redux/slices/user.slice';
import { useNavigate } from 'react-router-dom';

const url = process.env.REACT_APP_API_URL;

const Login: React.FC = () => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const form = useRef(null) as unknown as MutableRefObject<HTMLFormElement>;

  useEffect(() => {
    alert(`Hola, este es el usuario para probar la aplicación:
    email: user@mail.com
    password: admin123
    `);
  }, []);

  const handleSubmit: FormEventHandler = async (e) => {
    e.preventDefault();
    const formData = new FormData(form.current);
    const email = formData.get('email');
    const password = formData.get('password');
    try {
      const res = await axios.post(`${url}auth/login`, { email, password });
      if (res.status === 200) {
        dispatch(addUser(res.data));
        navigate('/user');
      }
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div className="flex flex-col items-center justify-center pt-36">
      <div className="bg-gray-500 p-6 rounded-xl">
        <h1 className="text-2xl text-white mb-4">Login</h1>
        <form
          ref={form}
          onSubmit={handleSubmit}
          className="flex flex-col gap-4 text-white"
        >
          <label htmlFor="email">Email: </label>
          <input
            type="text"
            id="email"
            name="email"
            className="bg-gray-500 border border-white rounded-md px-2 py-1"
          />
          <label htmlFor="password">Password: </label>
          <input
            type="password"
            id="password"
            name="password"
            className="bg-gray-500 border border-white rounded-md px-2 py-1"
          />
          <button className="bg-gray-500 border-2 font-semibold border-white text-white rounded-xl py-2 px-4 hover:bg-slate-200 hover:text-gray-700 mx-auto block mt-8 disabled:opacity-50">
            login
          </button>
        </form>
      </div>
    </div>
  );
};

export default Login;
