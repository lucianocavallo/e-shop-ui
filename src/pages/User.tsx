import axios from 'axios';
import { useEffect } from 'react';
import { useAppDispatch, useAppSelector } from '../redux/hooks';
import { cleanProducts, initProducts } from '../redux/slices/products.slice';
import { HiTrash } from 'react-icons/hi';
import { Link } from 'react-router-dom';

const url = process.env.REACT_APP_API_URL;

const User: React.FC = () => {
  const { list } = useAppSelector((state) => state.products);
  const user = useAppSelector((state) => state.user);
  const dispatch = useAppDispatch();

  const handleDeleteProduct = async (id: number) => {
    const config = { headers: { Authorization: `Bearer ${user.token}` } };
    const res = await axios.delete(`${url}products/${id}`, config);
    if (res.status === 200) {
      alert(`Producto con id: ${id} eliminado correctamente!`);
      dispatch(cleanProducts());
    }
  };

  useEffect(() => {
    (async () => {
      const { data } = await axios.get(`${url}products`);
      dispatch(initProducts(data));
    })();
  }, [dispatch, list.length]);

  return (
    <div>
      <h2 className="text-xl mb-4 text-center">Hello {user.user.email}</h2>
      <Link
        to="/user/create-product"
        className="bg-gray-500 border-2 font-semibold border-gray-500 text-white rounded-xl py-2 px-4 hover:bg-slate-200 hover:text-gray-700 mx-auto mt-8 disabled:opacity-50 mb-6 text-center block"
      >
        Create Product
      </Link>
      <ul className="w-96 border border-gray-700 p-2 rounded-2xl">
        {list.map((product) => (
          <li
            className="flex w-full justify-between items-center border-b"
            key={`product-${product.id}`}
          >
            <div className="flex justify-between flex-grow items-center">
              <div>
                <p>{product.name}</p>
                <p className="text-gray-700">${product.price}</p>
              </div>
              <figure className="w-16 h-16 flex itemx-center">
                <img
                  className="object-contain"
                  src={product.image_url}
                  alt="productImg"
                />
              </figure>
            </div>
            <div className="flex items-center">
              <button
                onClick={() => handleDeleteProduct(product.id)}
                className="ml-4 cursor-pointer"
              >
                <HiTrash color="red" />
              </button>
            </div>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default User;
