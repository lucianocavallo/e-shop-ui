import { useEffect, useState } from 'react';
import axios from 'axios';

import ProductCard from '../components/ProductCard';

import { useAppDispatch, useAppSelector } from '../redux/hooks';
import { addProducts, initProducts } from '../redux/slices/products.slice';

const url = process.env.REACT_APP_API_URL;
const limit = 5;

export default function ProductList() {
  const [disabled, setDisabled] = useState(false);
  const { list } = useAppSelector((state) => state.products);
  const dispatch = useAppDispatch();
  const offset = list.length;

  useEffect(() => {
    (async () => {
      if (!list.length) {
        const { data } = await axios.get(
          `${url}products?limit=${limit}&offset=${offset}`
        );
        dispatch(initProducts(data));
      }
    })();
  }, [dispatch, list.length, offset]);

  const handleLoadMore = async () => {
    const res = await axios.get(
      `${url}products?limit=${limit}&offset=${offset}`
    );
    dispatch(addProducts(res.data));
    if (res.data.length < limit) {
      setDisabled(true);
    }
  };

  return (
    <main>
      <div className="flex flex-wrap justify-center items-center md:flex-row lg:w-[800px] lg:mx-auto">
        {list.map((product) => (
          <ProductCard product={product} key={product.id} />
        ))}
      </div>
      <div>
        <button
          disabled={disabled}
          onClick={handleLoadMore}
          className="bg-gray-500 border-2 font-semibold border-gray-500 text-white rounded-xl py-2 px-4 hover:bg-slate-200 hover:text-gray-700 mx-auto block mt-8 disabled:opacity-50"
        >
          {disabled ? 'No more products' : 'Load More'}
        </button>
      </div>
    </main>
  );
}
