import React, {
  FormEventHandler,
  useRef,
  MutableRefObject,
  useState,
} from 'react';
import axios from 'axios';
import { useAppDispatch, useAppSelector } from '../redux/hooks';
import { useNavigate } from 'react-router-dom';
import Notice from '../components/Notice';
import { productValidator } from '../utils/productValidator';
import { cleanProducts } from '../redux/slices/products.slice';

const url = process.env.REACT_APP_API_URL;

const CreateProduct: React.FC = () => {
  const dispatch = useAppDispatch();
  const { token } = useAppSelector((state) => state.user);
  const navigate = useNavigate();
  const [error, setError] = useState('');
  const form = useRef(null) as unknown as MutableRefObject<HTMLFormElement>;

  const handleSubmit: FormEventHandler = async (e) => {
    e.preventDefault();
    const formData = new FormData(form.current);
    const product = {
      name: String(formData.get('name')),
      price: Number(formData.get('price')),
      image_url: String(formData.get('image_url')),
      description: String(formData.get('description')),
      brandId: Number(formData.get('brand')),
    };
    try {
      const validation = productValidator(product, setError);
      if (!validation) {
        setTimeout(() => setError(''), 3000);
        throw new Error('Validation error, look at the red notice in the UI');
      }
      const config = { headers: { Authorization: `Bearer ${token}` } };
      const res = await axios.post(`${url}products`, product, config);
      if (res.status === 201) {
        alert(`Producto ${product.name} creado!`);
        setTimeout(() => navigate('/'), 1000);
        dispatch(cleanProducts());
      }
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div className="flex flex-col items-center justify-center pt-36 mb-48">
      <div className="bg-gray-500 p-6 rounded-xl">
        <h1 className="text-2xl text-white mb-4">Create Product</h1>
        <form
          ref={form}
          onSubmit={handleSubmit}
          className="flex flex-col gap-4 text-white"
        >
          <label htmlFor="name">Name: </label>
          <input
            type="text"
            id="name"
            name="name"
            className="bg-gray-500 border border-white rounded-md px-2 py-1"
          />
          <label htmlFor="price">Price: </label>
          <input
            type="price"
            id="price"
            name="price"
            className="bg-gray-500 border border-white rounded-md px-2 py-1"
          />
          <label htmlFor="image_url">Image URL: </label>
          <input
            type="image_url"
            id="image_url"
            name="image_url"
            className="bg-gray-500 border border-white rounded-md px-2 py-1"
          />
          <label htmlFor="description">Description: </label>
          <input
            type="description"
            id="description"
            name="description"
            className="bg-gray-500 border border-white rounded-md px-2 py-1"
          />
          <label htmlFor="brand">Brand: </label>
          <select name="brand" id="brand" className="py-2 text-gray-700">
            <option value="">Select a Brand</option>
            <option value="1">Dell</option>
            <option value="2">Apple</option>
            <option value="3">Asus</option>
            <option value="4">Bhango</option>
            <option value="5">Gigabyte</option>
            <option value="6">HP</option>
            <option value="7">Lenovo</option>
            <option value="8">MSI</option>
          </select>
          {error && <Notice message={error} />}
          <button
            type="submit"
            className="bg-gray-500 border-2 font-semibold border-white text-white rounded-xl py-2 px-4 hover:bg-slate-200 hover:text-gray-700 mx-auto block mt-8 disabled:opacity-50"
          >
            create product
          </button>
        </form>
      </div>
    </div>
  );
};

export default CreateProduct;
