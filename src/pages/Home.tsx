import { Link } from 'react-router-dom';

const Home: React.FC = () => {
  return (
    <div>
      <h1 className="text-4xl font-bold text-center mt-36">
        Wellcome to E-Shop!
        <br />
        your notebook's <br />
        e-commerce
      </h1>
      <button className="bg-gray-500 border-2 font-semibold border-gray-500 text-white rounded-xl py-2 px-4 hover:bg-slate-200 hover:text-gray-700 mx-auto block mt-8">
        <Link to="/products">Go to Products</Link>
      </button>
    </div>
  );
};
export default Home;
