interface Product {
  id: number;
  createdAt: Date;
  price: number;
  name: string;
  image_url: string;
  description: string;
  brandId: number;
  brand: Brand;
}
interface ProductValidation {
  price: number;
  name: String;
  image_url: String;
  description: String;
  brandId: number;
}

type Brand = {
  image_url: string;
  name: string;
};
