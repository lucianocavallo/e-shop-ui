import { Link } from 'react-router-dom';
import { HiPlusCircle } from 'react-icons/hi';

const ProductCard: React.FC<ProductCardProps> = ({ product }) => {
  return (
    <>
      <Link
        to={`/products/${product.id}`}
        className="w-60 h-auto m-1 p-3 shadow-xl shadow-gray-300"
      >
        <figure className="w-full h-60 flex justify-center">
          <img
            className="object-contain"
            src={product.image_url}
            alt="productImg"
          />
        </figure>
        <div className="py-3 space-y-1 relative">
          <h3 className="text-xl font-medium">
            $
            {new Intl.NumberFormat('es-ES', {
              style: 'currency',
              currency: 'ARP',
            }).format(product.price)}
          </h3>
          <p className="text-lg text-gray-500 font-medium">{product.name}</p>
          <button className="absolute top-2 right-2">
            <HiPlusCircle size={30} />
          </button>
        </div>
      </Link>
    </>
  );
};

export default ProductCard;

type ProductCardProps = {
  product: Product;
};
