import { HiShoppingCart } from 'react-icons/hi';
import { Link } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../redux/hooks';
import { removeUser } from '../redux/slices/user.slice';

export default function Header() {
  const dispatch = useAppDispatch();
  const user = useAppSelector((state) => state.user);

  const handleLogout = () => {
    dispatch(removeUser());
  };

  return (
    <header className="w-full h-24 bg-gray-700">
      <nav className="h-full px-4 flex items-center justify-between lg:w-[800px] lg:mx-auto">
        <Link to="/">
          <h1 className="text-white text-3xl m-0 p-0 lg:hidden">ES</h1>
          <h1 className="text-white text-3xl m-0 p-0 hidden lg:block">EShop</h1>
        </Link>
        <div className="flex space-x-4 items-center mr-2">
          <div className="flex flex-col space-x-2 text-lg text-white">
            {user?.token && <Link to="/user">{user.user.email}</Link>}
            {!user?.token && (
              <p className="border border-white rounded-xl mt-1 hover:bg-white hover:text-gray-700 px-6">
                <Link to="login">login</Link>
              </p>
            )}
            {user?.token && (
              <button
                onClick={handleLogout}
                className="border border-white rounded-xl mt-1 hover:bg-white hover:text-gray-700"
              >
                logout
              </button>
            )}
          </div>
          <figure className="relative">
            <HiShoppingCart size={30} color="white" />
            <span className="w-4 h-4 bg-gray-400 rounded-full absolute -right-1 -top-2"></span>
          </figure>
        </div>
      </nav>
    </header>
  );
}
