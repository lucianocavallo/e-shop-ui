import Header from './Header';

const AppLayout: React.FC = ({ children }) => {
  return (
    <main className="bg-sky-50 min-h-screen">
      <Header />
      <section className="p-6 flex items-center justify-center">
        {children}
      </section>
    </main>
  );
};

export default AppLayout;
