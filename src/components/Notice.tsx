const Notice: React.FC<NoticeProps> = ({ message }) => {
  return (
    <div className="text-white bg-red-400 w-full h-auto text-center rounded-sm py-1">
      <p>{message}</p>
    </div>
  );
};

export default Notice;

type NoticeProps = {
  message: string;
};
