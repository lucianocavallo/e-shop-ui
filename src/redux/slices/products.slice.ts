import { createSlice, PayloadAction } from '@reduxjs/toolkit';
interface ProductsState {
  list: Product[];
  cart: Product[];
}

const initialState: ProductsState = {
  list: [],
  cart: [],
};

const productsSlice = createSlice({
  name: 'products',
  initialState,
  reducers: {
    initProducts(state, action: PayloadAction<Product[]>) {
      return {
        ...state,
        list: action.payload,
      };
    },
    addProducts(state, action) {
      return { ...state, list: state.list.concat(action.payload) };
    },
    addProduct(state, action: PayloadAction<Product>) {
      state.list.push(action.payload);
      return {
        ...state,
        list: state.list,
      };
    },
    cleanProducts(state) {
      return {
        list: [],
        cart: [],
      };
    },
  },
});

export const { initProducts, addProduct, addProducts, cleanProducts } =
  productsSlice.actions;
export default productsSlice.reducer;
