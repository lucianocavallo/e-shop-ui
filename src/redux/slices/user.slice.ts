import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface User {
  token: string;
  user: {
    id: number;
    email: string;
    username: string;
  };
}

const initialState: User = {
  token: '',
  user: {
    id: 0,
    email: '',
    username: '',
  },
};

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    addUser(state, action: PayloadAction<User>) {
      state.token = action.payload.token;
      state.user = action.payload.user;
    },
    removeUser(state) {
      state.token = '';
      state.user = {
        id: 0,
        email: '',
        username: '',
      };
    },
  },
});

export const { addUser, removeUser } = userSlice.actions;
export default userSlice.reducer;
