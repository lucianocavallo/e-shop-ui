import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useAppSelector } from './redux/hooks';

import AppLayout from './components/AppLayout';
import Home from './pages/Home';
import User from './pages/User';
import Login from './pages/Login';
import ProductList from './pages/ProductList';
import ProductDetail from './pages/ProductDetail';
import CreateProduct from './pages/CreateProduct';

function App() {
  const user = useAppSelector((state) => state.user);

  return (
    <BrowserRouter>
      <AppLayout>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/products" element={<ProductList />} />
          <Route path="/products/:id" element={<ProductDetail />} />
          <Route path="/login" element={<Login />} />
          <Route path="/user" element={user?.token ? <User /> : <Login />} />
          <Route
            path="/user/create-product"
            element={user?.token ? <CreateProduct /> : <Login />}
          />
        </Routes>
      </AppLayout>
    </BrowserRouter>
  );
}

export default App;
