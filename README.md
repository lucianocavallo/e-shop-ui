<div align="center">
<h1 align="center">E-Shop UI</h1>
</div>

## Solution:

This App with create-react-app using Tailwind as CSS framework, and was written in TypeScript.
The App's directories are organized basically in a pages dir that contains all the App's pages, then the components dir that are used inside the pages. In the redux dir we encounter all the redux logic as the store and the configureApp with it's options.
The requests to the backend are made with Axios.

### Built With

- [React.js](https://reactjs.org/)
- [Typescript](https://www.typescriptlang.org/)
- [Redux](https://redux.js.org/)
- [Redux Toolkit](https://redux-toolkit.js.org/)
- [Axios](https://axios-http.com/)
- [React Icons](https://react-icons.github.io/react-icons)
- [TailwindCSS](https://tailwindcss.com/)

<p align="right">(<a href="#top">back to top</a>)</p>

### Available actions:

Create and delete products: after a successful login, you're able to access the user page to delete products, or in the same page a button takes you to the create product page, which is a simple form that has a simple validation of the inputs to create a product.

### Authentication

The app stores the Json Web Token returned when making a successful login, which then is used to make the requests that require it.
